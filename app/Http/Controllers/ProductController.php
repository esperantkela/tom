<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    /**
     * affiche la liste des produits .
     *
     */
    public function index()
    {
        $products= Product::paginate(6);
        return view ('products.index')->with('products',$products);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($slug)
    {
        $products = Product::where('slug', $slug)->firstOrFail();

        return view('products.show')->with('product', $products);
    }

    public function search()
    {
        $q= resquest()->input('q');

        $products= Product::where('title', 'like', '%$q%')
                    ->orwhere('description', 'like', '%$q%')
                    ->paginate(6);
        return view('products.search', compact('products'));
    }
}
