
<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="e-commerce app">
    <meta name="author" content="Espérant Kela">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>@yield('title')</title>

    <link rel="canonical" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('bootstrap/font-awesome/css/font-awesome.min.css')}}">



    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{asset('bootstrap/css/blog.css')}}" rel="stylesheet">
  </head>
  <body>
    <div class="container">
  <header class="blog-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
      <div class="col-4 pt-1">
        <a class="text-muted" href="{{route('cart.index')}}">Panier <span class="badge badge-pill badge-info">{{Cart::count()}}</span></a>
      </div>
      <div class="col-4 text-center">
        <img src="{{asset('logo/Shopping.png')}}" alt="logo" width="50px" class="pr-4" />
        <a class="blog-header-logo text-dark" href="{{route('products.index')}}">Commande App</a>
      </div>
      <div class="col-4 d-flex justify-content-end align-items-center">
        @include('includes.search')
        <a class="btn btn-sm btn-outline-info" href="#">login</a>
      </div>
    </div>
  </header>

  <div class="nav-scroller py-1 mb-2">
    <nav class="nav d-flex justify-content-between">
      <a class="p-2 text-muted" href="#">Hi-Tech</a>
      <a class="p-2 text-muted" href="#">Design</a>
      <a class="p-2 text-muted" href="#">Classic</a>
      <a class="p-2 text-muted" href="#">Matériel</a>
      <a class="p-2 text-muted" href="#">Meubles</a>
      <a class="p-2 text-muted" href="#">Ordinateur</a>

    </nav>
  </div>

  @include('includes/messages-flash')

  <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
    <div class="col-md-6 px-0">
      <h1 class="display-4 font-italic">App Commande</h1>
      <p class="lead my-3">Commander librément  .</p>
      <p class="lead mb-0"><a href="#" class="text-white font-weight-bold">merci de nous avoir fait confiance...</a></p>
    </div>
  </div>

  <div class="row mb-2">
    @yield('content')
    </div>

  </div>
</div>


<footer class="blog-footer">
    <p>@Copyright <a href="https://wakeuptech.com/">Wake up Tech</a> |<a href="https://twitter.com/mdo">+243 891061159</a>.</p>
    <p>
      <a href="#">Retour vers le haut de la page</a>
    </p>
  </footer>
</body>
</html>
