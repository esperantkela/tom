
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="max-age=604800" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>app commande</title>

<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">

<!-- jQuery -->
<script src="{{asset('bootstrap/js/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('bootstrap/js/jquery-2.0.0.min.js')}}" type="text/javascript"></script>

<!-- Bootstrap4 files-->
<script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
<link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>

<!-- Font awesome 5 -->
<link href="{{asset('/bootstrap/font-awesome/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">

<!-- custom style -->
<link href="{{asset('css/ui.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('bootstrap/css/blog.css')}}" rel="stylesheet">
<link href="{{asset('css/responsive.css')}}" rel="stylesheet" media="only screen and (max-width: 1200px)" />

<!-- custom javascript -->
<script src="js/script.js" type="text/javascript"></script>

<script type="text/javascript">
/// some script

// jquery ready start
$(document).ready(function() {
	// jQuery code

});
// jquery end
</script>

</head>
<body>


<header class="section-header">

<nav class="navbar navbar-dark navbar-expand p-0 bg-primary">
<div class="container">
    <ul class="navbar-nav d-none d-md-flex mr-auto">
		<li class="nav-item"><a class="nav-link" href="#">Home</a></li>

    </ul>
    <ul class="navbar-nav">
		<li  class="nav-item"><a href="#" class="nav-link"> Appel: +243891061159 </a></li>
	</ul> <!-- list-inline //  -->
  </div> <!-- navbar-collapse .// -->
</div> <!-- container //  -->
</nav> <!-- header-top-light.// -->

<section class="header-main border-bottom">
	<div class="container">
<div class="row align-items-center">
	<div class="col-lg-2 col-6">
		<a href="{{route('products.index')}}" class="brand-wrap">
            <img class="logo" src="{{asset('logo/Shopping.png')}}">
            NomAPP
		</a> <!-- brand-wrap.// -->
	</div>
    @include('includes.search')
	<div class="col-lg-4 col-sm-6 col-12">
		<div class="widgets-wrap float-md-right">
			<div class="widget-header  mr-3">
				<a href="{{route('cart.index')}}" class="icon icon-sm rounded-circle border"><i class="fa fa-shopping-cart"></i>
				<span class="badge badge-pill badge-danger notify">{{Cart::count()}}</span></a>
			</div>
			<div class="widget-header icontext">
				<a href="#" class="icon icon-sm rounded-circle border"><i class="fa fa-user"></i></a>
				<div class="text">
					<span class="text-muted">Bienvenue !</span>
					<div>
						<a href="#">s'incrire</a> |
						<a href="#"> se connecter</a>
					</div>
				</div>
			</div>
		</div> <!-- widgets-wrap.// -->
	</div> <!-- col.// -->
</div> <!-- row.// -->
	</div> <!-- container.// -->
</section> <!-- header-main .// -->
</header> <!-- section-header.// -->

</header> <!-- section-header.// -->

<body>
    <div class="container">
  <header class="blog-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
      <div class="col-4 d-flex justify-content-end align-items-center">


      </div>
    </div>
  </header>

  <div class="nav-scroller py-1 mb-2">
    <nav class="nav d-flex justify-content-between">
      <a class="p-2 text-muted" href="#">Hi-Tech</a>
      <a class="p-2 text-muted" href="#">Design</a>
      <a class="p-2 text-muted" href="#">Classic</a>
      <a class="p-2 text-muted" href="#">Matériel</a>
      <a class="p-2 text-muted" href="#">Meubles</a>
      <a class="p-2 text-muted" href="#">Ordinateur</a>

    </nav>
  </div>

  @include('includes/messages-flash')

  <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
    <div class="col-md-6 px-0">
      <h1 class="display-4 font-italic">App Commande</h1>
      <p class="lead my-3">Commander librément et en toute sécurité  .</p>
      <p class="lead mb-0"><a href="#" class="text-white font-weight-bold">merci de nous avoir fait confiance...</a></p>
    </div>
  </div>

  <div class="row mb-2">
    @yield('content')
    </div>

  </div>
</div>


<footer class="blog-footer">
    <p>@Copyright <a href="https://wakeuptech.com/">Wake up Tech</a> |<a href="">+243 891061159</a>.</p>
    <p>
      <a href="#">Retour vers le haut de la page</a>
    </p>
  </footer>





</body>
</html>

