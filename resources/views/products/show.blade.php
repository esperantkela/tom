@extends('layouts.master')

@section('content')

<div class="col-md-12">
    <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
      <div class="col p-4 d-flex flex-column position-static">
        <strong class="d-inline-block mb-2 text-success">Categorie1</strong>
        <h5 class="mb-0">{{$product->title}}</h5>
        <div class="mb-1 text-muted">{{$product->created_at->format('d/m/Y')}}</div>
        <p class="mb-auto">{{$product->description}}</p>
        <p class="mb-auto"><strong>{{$product->getPrice()}}</strong></p>
        <form action="{{route('cart.store')}}" method="POST">
            <button type="submit" class="btn btn-success"><i class="fa fa-shopping-cart"> Ajouter au panier</i></button>
            @csrf
            <input type="hidden" name="product_id" value="{{$product->id}}">

        </form>
      </div>
      <div class="col-auto d-none d-lg-block">
        <img src="{{$product->image}}">
      </div>
    </div>
  </div>

@endsection
