
<div class="col-lg-6 col-12 col-sm-12">
    <form action="{{route('products.search')}}" class="search">
        <div class="input-group w-100">
            <input type="text" class="form-control" name="q" placeholder="Rechercher un produit">
            <div class="input-group-append">
              <button class="btn btn-primary" type="submit">
                <i class="fa fa-search"></i>
              </button>
            </div>
        </div>
    </form> <!-- search-wrap .end// -->
</div> <!-- col.// -->

